------------------------------------------------------------------------------------------------------------------------------------------
-- SuperMetroid_HitBox_snes9x_1.60.lua By UMD 18.09.2019
-- Compactable with snes9x_1.53 and higher
-- =======================================================================================================================================

-------------------------------------------------------------------------------------------------------------------------------
--SYNC CONTROLS: Ctrl+down to on/off sync, if disabled: alt+(up,down,left,right) to move screen AND ctrl+up to resset it to 0--
-------------------------------------------------------------------------------------------------------------------------------

--Config
local DebugFlag = false						--bts print per block
local BGGrid 	= true						--grid on air
local BGgame 	= false						--blackout game
local IGtime  	= true						--Draw IG time
local EnemyDr 	= true						--enemy drawing with hitboxes and hp
local Proj 		= true						--Draw projectles with damage
local CamSync 	= true						--sync ign camera with hitboxes
local XrayBlocks= false						--Draw xray trick blocks
local RegBlocks = {0x09,0x0B,0x0C,0x0A} 	--(DOOR(id),Crumple,Shoot)
local rest = false 							--Sync resset var (DO NOT TOUCH THIS)
local SyncState = false 					--Sync state var (DO NOT TOUCH THIS)
local syncX,syncY = 0,0 					--Default pos of desync screen
local SyncSpeed = 5							--Speed of move screen

-- ==============================================================SPECIAL FUNCS=================================================================

--[[http://drewseph.zophar.net/Kejardon/BlockCollisReact.txt
|Vertical   =Vert
|Horizontal =		 Horz
|BlockType	|Vert	|Horz	|
|			|		|		|
|0x00		|8F47	|8F47	| (Air)
|0x01		|8FDA	|8FBB	| (Slope_Slp)
|0x02		|901A	|9018	| ATX(Air, tricks, X-ray)
|0x03		|909D	|906F	| Treadmill(Trd)
|0x04		|8F47	|8F47	| (Shootable Air)
|0x05		|9411	|9411	| (Horizontal extend)
|0x06		|8F47	|8F47	| X-ray Fluoroscopy is Impossible(XFI)
|0x07		|9313	|92F9	| BaA(Bombable, Air = Bomb bubble sort to Air)(ON／OFF)
|0x08		|8F82	|8F49	|(Solid)
|0x09		|93CE	|938B	|(Door)
|0x0A		|905D	|904B	|(Spike)
|0x0B		|9102	|90CB	|(Crumble block)
|0x0C		|8F82	|8F49	|(Stb=D,Door,Gate)
|0x0D		|9447	|9447	|(Vertical extend_Ver)
|0x0E		|8F82	|8F49	|(Grapple)
|0x0F		|934C	|932D	|(Bomb block)

]]

local function clamp(N,Min,Max)
	if N < Min then return Min 
	elseif N > Max then return Max 
	else return N end
end
local function colorA(R,G,B,A)
	local t = {R,G,B,A}
	local var = 0
	for i, k in ipairs(t) do
		var = var+ bit.lshift(clamp(k,0,255),24-(i-1)*8)
	end
	return var
end
local function color(R,G,B)
	local t = {R,G,B}
	local var = 0
	for i, k in ipairs(t) do
		var = var+ bit.lshift(clamp(k,0,255),24-(i-1)*8)
	end
	return var+255
end
local function box2(Xx, Yy, rX, rY, WHAT_IS_IT, C1, C2, AND_IT)
	local SdX, SdY = Xx + rX, Yy + rY
	gui.box(Xx, Yy, SdX, SdY, WHAT_IS_IT, C1, C2)
	
end
local function text2(X, Y, TEXT, COLOR)
	X,Y = X+2,Y+3
	gui.text(X, Y, TEXT,COLOR)
end
local function boolToNum(var)
  return var and 1 or 0
end

-- blocks ============================================================================================================================================
blocks = {[0x02]=true,[0x09]=true}
-- doors =============================================================================================================================================
doors = { --in original game(not romhack)
	[0x88FE]=true, [0x890A]=true, [0x8916]=true, [0x8922]=true, [0x892E]=true, [0x893A]=true, [0x8946]=true, [0x8952]=true,
	[0x895E]=true, [0x896A]=true, [0x8976]=true, [0x8982]=true, [0x898E]=true, [0x899A]=true, [0x89A6]=true, [0x89B2]=true,
	[0x89BE]=true, [0x89CA]=true, [0x89D6]=true, [0x89E2]=true, [0x89EE]=true, [0x89FA]=true, [0x8A06]=true, [0x8A12]=true,
	[0x8A1E]=true, [0x8A2A]=true, [0x8A36]=true, [0x8A42]=true, [0x8A4E]=true, [0x8A5A]=true, [0x8A66]=true, [0x8A72]=true,
	[0x8A7E]=true, [0x8A8A]=true, [0x8A96]=true, [0x8AA2]=true, [0x8AAE]=true, [0x8ABA]=true, [0x8AC6]=true, [0x8AD2]=true,
	[0x8ADE]=true, [0x8AEA]=true, [0x8AF6]=true, [0x8B02]=true, [0x8B0E]=true, [0x8B1A]=true, [0x8B26]=true, [0x8B32]=true,
	[0x8B3E]=true, [0x8B4A]=true, [0x8B56]=true, [0x8B62]=true, [0x8B6E]=true, [0x8B7A]=true, [0x8B86]=true, [0x8B92]=true,
	[0x8B9E]=true, [0x8BAA]=true, [0x8BB6]=true, [0x8BC2]=true, [0x8BCE]=true, [0x8BDA]=true, [0x8BE6]=true, [0x8BF2]=true,
	[0x8BFE]=true, [0x8C0A]=true, [0x8C16]=true, [0x8C22]=true, [0x8C2E]=true, [0x8C3A]=true, [0x8C46]=true, [0x8C52]=true,
	[0x8C5E]=true, [0x8C6A]=true, [0x8C76]=true, [0x8C82]=true, [0x8C8E]=true, [0x8C9A]=true, [0x8CA6]=true, [0x8CB2]=true,
	[0x8CBE]=true, [0x8CCA]=true, [0x8CD6]=true, [0x8CE2]=true, [0x8CEE]=true, [0x8CFA]=true, [0x8D06]=true, [0x8D12]=true,
	[0x8D1E]=true, [0x8D2A]=true, [0x8D36]=true, [0x8D42]=true, [0x8D4E]=true, [0x8D5A]=true, [0x8D66]=true, [0x8D72]=true,
	[0x8D7E]=true, [0x8D8A]=true, [0x8D96]=true, [0x8DA2]=true, [0x8DAE]=true, [0x8DBA]=true, [0x8DC6]=true, [0x8DD2]=true,
	[0x8DDE]=true, [0x8DEA]=true, [0x8DF6]=true, [0x8E02]=true, [0x8E0E]=true, [0x8E1A]=true, [0x8E26]=true, [0x8E32]=true,
	[0x8E3E]=true, [0x8E4A]=true, [0x8E56]=true, [0x8E62]=true, [0x8E6E]=true, [0x8E7A]=true, [0x8E86]=true, [0x8E92]=true,
	[0x8E9E]=true, [0x8EAA]=true, [0x8EB6]=true, [0x8EC2]=true, [0x8ECE]=true, [0x8EDA]=true, [0x8EE6]=true, [0x8EF2]=true,
	[0x8EFE]=true, [0x8F0A]=true, [0x8F16]=true, [0x8F22]=true, [0x8F2E]=true, [0x8F3A]=true, [0x8F46]=true, [0x8F52]=true,
	[0x8F5E]=true, [0x8F6A]=true, [0x8F76]=true, [0x8F82]=true, [0x8F8E]=true, [0x8F9A]=true, [0x8FA6]=true, [0x8FB2]=true,
	[0x8FBE]=true, [0x8FCA]=true, [0x8FD6]=true, [0x8FE2]=true, [0x8FEE]=true, [0x8FFA]=true, [0x9006]=true, [0x9012]=true,
	[0x901E]=true, [0x902A]=true, [0x9036]=true, [0x9042]=true, [0x904E]=true, [0x905A]=true, [0x9066]=true, [0x9072]=true,
	[0x907E]=true, [0x908A]=true, [0x9096]=true, [0x90A2]=true, [0x90AE]=true, [0x90BA]=true, [0x90C6]=true, [0x90D2]=true,
	[0x90DE]=true, [0x90EA]=true, [0x90F6]=true, [0x9102]=true, [0x910E]=true, [0x911A]=true, [0x9126]=true, [0x9132]=true,
	[0x913E]=true, [0x914A]=true, [0x9156]=true, [0x9162]=true, [0x916E]=true, [0x917A]=true, [0x9186]=true, [0x9192]=true,
	[0x919E]=true, [0x91AA]=true, [0x91B6]=true, [0x91C2]=true, [0x91CE]=true, [0x91DA]=true, [0x91E6]=true, [0x91F2]=true,
	[0x91FE]=true, [0x920A]=true, [0x9216]=true, [0x9222]=true, [0x922E]=true, [0x923A]=true, [0x9246]=true, [0x9252]=true,
	[0x925E]=true, [0x926A]=true, [0x9276]=true, [0x9282]=true, [0x928E]=true, [0x929A]=true, [0x92A6]=true, [0x92B2]=true,
	[0x92BE]=true, [0x92CA]=true, [0x92D6]=true, [0x92E2]=true, [0x92EE]=true, [0x92FA]=true, [0x9306]=true, [0x9312]=true,
	[0x931E]=true, [0x932A]=true, [0x9336]=true, [0x9342]=true, [0x934E]=true, [0x935A]=true, [0x9366]=true, [0x9372]=true,
	[0x937E]=true, [0x938A]=true, [0x9396]=true, [0x93A2]=true, [0x93AE]=true, [0x93BA]=true, [0x93C6]=true, [0x93D2]=true,
	[0x93DE]=true, [0x93EA]=true, [0x93F6]=true, [0x9402]=true, [0x940E]=true, [0x941A]=true, [0x9426]=true, [0x9432]=true,
	[0x943E]=true, [0x944A]=true, [0x9456]=true, [0x9462]=true, [0x946E]=true, [0x947A]=true, [0x9486]=true, [0x9492]=true,
	[0x949E]=true, [0x94AA]=true, [0x94B6]=true, [0x94C2]=true, [0x94CE]=true, [0x94DA]=true, [0x94E6]=true, [0x94F2]=true,
	[0x94FE]=true, [0x950A]=true, [0x9516]=true, [0x9522]=true, [0x952E]=true, [0x953A]=true, [0x9546]=true, [0x9552]=true,
	[0x955E]=true, [0x956A]=true, [0x9576]=true, [0x9582]=true, [0x958E]=true, [0x959A]=true, [0x95A6]=true, [0x95B2]=true,
	[0x95BE]=true, [0x95CA]=true, [0x95D6]=true, [0x95E2]=true, [0x95EE]=true, [0x95FA]=true, [0x9606]=true, [0x9612]=true,
	[0x961E]=true, [0x962A]=true, [0x9636]=true, [0x9642]=true, [0x964E]=true, [0x965A]=true, [0x9666]=true, [0x9672]=true,
	[0x967E]=true, [0x968A]=true, [0x9696]=true, [0x96A2]=true, [0x96AE]=true, [0x96BA]=true, [0x96C6]=true, [0x96D2]=true,
	[0x96DE]=true, [0x96EA]=true, [0x96F6]=true, [0x9702]=true, [0x970E]=true, [0x971A]=true, [0x9726]=true, [0x9732]=true,
	[0x973E]=true, [0x974A]=true, [0x9756]=true, [0x9762]=true, [0x976E]=true, [0x977A]=true, [0x9786]=true, [0x9792]=true,
	[0x979E]=true, [0x97AA]=true, [0x97B6]=true, [0x97C2]=true, [0x97CE]=true, [0x97DA]=true, [0x97E6]=true, [0x97F2]=true,
	[0x97FE]=true, [0x980A]=true, [0x9816]=true, [0x9822]=true, [0x982E]=true, [0x983A]=true, [0x9846]=true, [0x9852]=true,
	[0x985E]=true, [0x986A]=true, [0x9876]=true, [0x9882]=true, [0x988E]=true, [0x989A]=true, [0x98A6]=true, [0x98B2]=true,
	[0x98BE]=true, [0x98CA]=true, [0x98D6]=true, [0x98E2]=true, [0x98EE]=true, [0x98FA]=true, [0x9906]=true, [0x9912]=true,
	[0x991E]=true, [0x992A]=true, [0x9936]=true, [0x9942]=true, [0x994E]=true, [0x995A]=true, [0x9966]=true, [0x9972]=true,
	[0x997E]=true, [0x998A]=true, [0x9996]=true, [0x99A2]=true, [0x99AE]=true, [0x99BA]=true, [0x99C6]=true, [0x99D2]=true,
	[0x99DE]=true, [0x99EA]=true, [0x99F6]=true, [0x9A02]=true, [0x9A0E]=true, [0x9A1A]=true, [0x9A26]=true, [0x9A32]=true,
	[0x9A3E]=true, [0x9A4A]=true, [0x9A56]=true, [0x9A62]=true, [0x9A6E]=true, [0x9A7A]=true, [0x9A86]=true, [0x9A92]=true,
	[0x9A9E]=true, [0x9AAA]=true, [0x9AB6]=true, [0xA18C]=true, [0xA198]=true, [0xA1A4]=true, [0xA1B0]=true, [0xA1BC]=true,
	[0xA1C8]=true, [0xA1D4]=true, [0xA1E0]=true, [0xA1EC]=true, [0xA1F8]=true, [0xA204]=true, [0xA210]=true, [0xA21C]=true,
	[0xA228]=true, [0xA234]=true, [0xA240]=true, [0xA24C]=true, [0xA258]=true, [0xA264]=true, [0xA270]=true, [0xA27C]=true,
	[0xA288]=true, [0xA294]=true, [0xA2A0]=true, [0xA2AC]=true, [0xA2B8]=true, [0xA2C4]=true, [0xA2D0]=true, [0xA2DC]=true,
	[0xA2E8]=true, [0xA2F4]=true, [0xA300]=true, [0xA30C]=true, [0xA318]=true, [0xA324]=true, [0xA330]=true, [0xA33C]=true,
	[0xA348]=true, [0xA354]=true, [0xA360]=true, [0xA36C]=true, [0xA378]=true, [0xA384]=true, [0xA390]=true, [0xA39C]=true,
	[0xA3A8]=true, [0xA3B4]=true, [0xA3C0]=true, [0xA3CC]=true, [0xA3D8]=true, [0xA3E4]=true, [0xA3F0]=true, [0xA3FC]=true,
	[0xA408]=true, [0xA414]=true, [0xA420]=true, [0xA42C]=true, [0xA438]=true, [0xA444]=true, [0xA450]=true, [0xA45C]=true,
	[0xA468]=true, [0xA474]=true, [0xA480]=true, [0xA48C]=true, [0xA498]=true, [0xA4A4]=true, [0xA4B0]=true, [0xA4BC]=true,
	[0xA4C8]=true, [0xA4D4]=true, [0xA4E0]=true, [0xA4EC]=true, [0xA4F8]=true, [0xA504]=true, [0xA510]=true, [0xA51C]=true,
	[0xA528]=true, [0xA534]=true, [0xA540]=true, [0xA54C]=true, [0xA558]=true, [0xA564]=true, [0xA570]=true, [0xA57C]=true,
	[0xA588]=true, [0xA594]=true, [0xA5A0]=true, [0xA5AC]=true, [0xA5B8]=true, [0xA5C4]=true, [0xA5D0]=true, [0xA5DC]=true,
	[0xA5E8]=true, [0xA5F4]=true, [0xA600]=true, [0xA60C]=true, [0xA618]=true, [0xA624]=true, [0xA630]=true, [0xA63C]=true,
	[0xA648]=true, [0xA654]=true, [0xA660]=true, [0xA66C]=true, [0xA678]=true, [0xA684]=true, [0xA690]=true, [0xA69C]=true,
	[0xA6A8]=true, [0xA6B4]=true, [0xA6C0]=true, [0xA6CC]=true, [0xA6D8]=true, [0xA6E4]=true, [0xA6F0]=true, [0xA6FC]=true,
	[0xA708]=true, [0xA714]=true, [0xA720]=true, [0xA72C]=true, [0xA738]=true, [0xA744]=true, [0xA750]=true, [0xA75C]=true,
	[0xA768]=true, [0xA774]=true, [0xA780]=true, [0xA78C]=true, [0xA798]=true, [0xA7A4]=true, [0xA7B0]=true, [0xA7BC]=true,
	[0xA7C8]=true, [0xA7D4]=true, [0xA7E0]=true, [0xA7EC]=true, [0xA7F8]=true, [0xA810]=true, [0xA828]=true, [0xA834]=true,
	[0xA840]=true, [0xA84C]=true, [0xA858]=true, [0xA864]=true, [0xA870]=true, [0xA87C]=true, [0xA888]=true, [0xA894]=true,
	[0xA8A0]=true, [0xA8AC]=true, [0xA8B8]=true, [0xA8C4]=true, [0xA8D0]=true, [0xA8DC]=true, [0xA8E8]=true, [0xA8F4]=true,
	[0xA900]=true, [0xA90C]=true, [0xA918]=true, [0xA924]=true, [0xA930]=true, [0xA93C]=true, [0xA948]=true, [0xA954]=true,
	[0xA960]=true, [0xA96C]=true, [0xA978]=true, [0xA984]=true, [0xA990]=true, [0xA99C]=true, [0xA9A8]=true, [0xA9B4]=true,
	[0xA9C0]=true, [0xA9CC]=true, [0xA9D8]=true, [0xA9E4]=true, [0xA9F0]=true, [0xA9FC]=true, [0xAA08]=true, [0xAA14]=true,
	[0xAA20]=true, [0xAA2C]=true, [0xAA38]=true, [0xAA44]=true, [0xAA50]=true, [0xAA5C]=true, [0xAA68]=true, [0xAA74]=true,
	[0xAA80]=true, [0xAA8C]=true, [0xAA98]=true, [0xAAA4]=true, [0xAAB0]=true, [0xAABC]=true, [0xAAC8]=true, [0xAAD4]=true,
	[0xAAE0]=true, [0xAAEC]=true, [0xAAF8]=true, [0xAB04]=true, [0xAB10]=true, [0xAB1C]=true, [0xAB28]=true, [0xAB34]=true,
	[0xAB40]=true, [0xAB4C]=true, [0xAB58]=true, [0xAB64]=true, [0xAB70]=true, [0xAB7C]=true, [0xAB88]=true, [0xAB94]=true,
	[0xABA0]=true, [0xABAC]=true, [0xABB8]=true, [0xABC4]=true, [0xABCF]=true, [0xABDA]=true, [0xABE5]=true
}
-- slope =============================================================================================================================================
slope = {
	-- -------------------------------------------------------------------------------------------
	[0x00] = function ()
		gui.box(TileX, TileY+8*VFlip, TileX+15*HFlip, TileY+15*VFlip, colorA(0,0,0,0), "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x01] = function ()
		gui.box(TileX+8*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, colorA(0,0,0,0), "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x02] = function ()
		gui.box(TileX+8*HFlip, TileY+8*VFlip, TileX+15*HFlip, TileY+15*VFlip, colorA(0,0,0,0), "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x03] = function ()
		gui.line(TileX+8*HFlip, TileY, TileX+15*HFlip, TileY, "Chartreuse") 
		gui.line(TileX+8*HFlip, TileY, TileX+8*HFlip, TileY+7*VFlip, "Chartreuse") 
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+8*VFlip, TileX+7*HFlip, TileY+8*VFlip,"Chartreuse") 
		gui.line(TileX, TileY+8*VFlip, TileX, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x05] = function ()
		gui.line(TileX, TileY+15*VFlip, TileX+7*HFlip, TileY+8*VFlip, "Chartreuse") 
		gui.line(TileX+8*HFlip, TileY+8*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x06] = function ()
		gui.line(TileX, TileY+15*VFlip, TileX+7*HFlip, TileY, "Chartreuse") 
		gui.line(TileX+8*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x07] = function ()
		gui.box(TileX, TileY+8*VFlip, TileX+15*HFlip, TileY+15*VFlip, colorA(0,0,0,0), "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x0E] = function ()
		gui.line(TileX, TileY+13*VFlip, TileX+13*HFlip, TileY, "Chartreuse") 
		gui.line(TileX, TileY+13*VFlip, TileX, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX+13*HFlip, TileY, TileX+15*HFlip, TileY, "Chartreuse") 
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x0F] = function ()
		gui.line(TileX, TileY+15*VFlip, TileX+5*HFlip, TileY+14*VFlip, "Chartreuse") 
		gui.line(TileX+6*HFlip, TileY+13*VFlip, TileX+9*HFlip, TileY+12*VFlip, "Chartreuse") 
		gui.line(TileX+10*HFlip, TileY+11*VFlip, TileX+11*HFlip, TileY+10*VFlip, "Chartreuse") 
 		gui.line(TileX+12*HFlip, TileY+9*VFlip, TileX+13*HFlip, TileY+6*VFlip, "Chartreuse") 
		gui.line(TileX+14*HFlip, TileY+5*VFlip, TileX+15*HFlip, TileY, "Chartreuse") 
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x12] = function ()
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY, "Chartreuse") 
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	-- Outline[0x01] slopefunction
	[0x13] = function ()
		gui.box(TileX, TileY, TileX+15*HFlip, TileY+15*VFlip, colorA(0,0,0,0), "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x14] = function ()
		gui.line(TileX+8*HFlip, TileY+15*VFlip, TileX+15*HFlip, TileY+8*VFlip, "Chartreuse") 
		gui.line(TileX+15*HFlip, TileY+8*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX+8*HFlip, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x15] = function ()
		gui.line(TileX+8*HFlip, TileY, TileX+15*HFlip, TileY, "Chartreuse") 
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+7*VFlip, TileX+7*HFlip, TileY, "Chartreuse") 
		gui.line(TileX, TileY+8*VFlip, TileX, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x16] = function ()
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+8*VFlip, "Chartreuse") 
		gui.line(TileX+15*HFlip, TileY+8*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x17] = function ()
		gui.line(TileX, TileY+7*VFlip, TileX+15*HFlip, TileY, "Chartreuse") 
		gui.line(TileX, TileY+8*VFlip, TileX, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x18] = function ()
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+11*VFlip, "Chartreuse") 
		gui.line(TileX+15*HFlip, TileY+11*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x19] = function ()
		gui.line(TileX, TileY+10*VFlip, TileX+15*HFlip, TileY+5*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+10*VFlip, TileX, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX+15*HFlip, TileY+5*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
	end,
	-- -------------------------------------------------------------------------------------------
	[0x1A] = function ()
		gui.line(TileX, TileY+4*VFlip, TileX+15*HFlip, TileY, "Chartreuse") 
		gui.line(TileX, TileY+4*VFlip, TileX, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse") 
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse")
	end,
	-- -------------------------------------------------------------------------------------------
	[0x1B] = function ()
		gui.line(TileX+8*HFlip, TileY+15*VFlip, TileX+15*HFlip, TileY, "Chartreuse")
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse")
		gui.line(TileX+8*HFlip, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse")
	end,
	-- -------------------------------------------------------------------------------------------
	[0x1C] = function ()
		gui.line(TileX, TileY+15*VFlip, TileX+7*HFlip, TileY, "Chartreuse")
		gui.line(TileX+8*HFlip, TileY, TileX+15*HFlip, TileY, "Chartreuse")
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse")
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse")
	end,
	-- -------------------------------------------------------------------------------------------
	[0x1D] = function ()
		gui.line(TileX+10*HFlip, TileY+15*VFlip, TileX+15*HFlip, TileY, "Chartreuse")
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse")
 		gui.line(TileX+10*HFlip, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse")
	end,
	-- -------------------------------------------------------------------------------------------
	[0x1E] = function ()
		gui.line(TileX+5*HFlip, TileY+15*VFlip, TileX+10*HFlip, TileY, "Chartreuse")
		gui.line(TileX+10*HFlip, TileY, TileX+15*HFlip, TileY, "Chartreuse")
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse")
		gui.line(TileX+5*HFlip, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse")
	end,
	-- -------------------------------------------------------------------------------------------
	[0x1F] = function ()
		gui.line(TileX, TileY+15*VFlip, TileX+5*HFlip, TileY, "Chartreuse")
		gui.line(TileX+5*HFlip, TileY, TileX+15*HFlip, TileY, "Chartreuse")
		gui.line(TileX+15*HFlip, TileY, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse")
		gui.line(TileX, TileY+15*VFlip, TileX+15*HFlip, TileY+15*VFlip, "Chartreuse")
	end
	-- -------------------------------------------------------------------------------------------
}
-- outline ===========================================================================================================================================
outline = {
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x00] = function () --(Air)
		if BGGrid then
			gui.line(TileX, TileY, TileX, TileY, color(170, 170, 170))
		end
	end,
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x01] = function () --Slope_Slp
		local b = memory.readbyte(BTS)
		if bit.band(b, 0x40) ~= 0 then
			TileX = TileX+15
			HFlip = -1
			else
			HFlip = 1
		end
		-- Inverts the horizontal co-ordinates of the lines
		if bit.band(b, 0x80) ~= 0 then
			TileY = TileY+15
			VFlip = -1
			else
			VFlip = 1
		end
		-- Inverts the vertical co-ordinates of the lines
		slopefunction = slope[bit.band(memory.readbyte(BTS), 0x1F)] or function ()
			gui.box(TileX, TileY, TileX+15*HFlip, TileY+15*VFlip,1 , "Chartreuse") 
		end
		slopefunction()
    	end, --Slope_Slp
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x02] = function () -- ATX(Air, tricks, X-ray)
		if XrayBlocks then
			--BTS
			--0x02 - spike
			box2(TileX, TileY, 16, 16, 1, colorA(100, 50, 100,150), color(85, 0, 0), -1)
			text2(TileX, TileY-1, "X", colorA(100, 0, 0,1000))
		else
			gui.line(TileX, TileY, TileX, TileY, color(170, 170, 170))
		end
	end, -- Air, tricks X-ray
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x03] = function () -- Treadmill(Trd)
		box2(TileX, TileY, 16, 16, 1, color(170, 0, 0), color(85, 0, 0), -1)
		text2(TileX+4, TileY-1, "T", color(128, 128, 128))
	end, -- Treadmill
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x04] = function ()  -- Shootable Air
		box2(TileX, TileY, 16, 16, 1, color(0, 170, 0), color(0, 85, 0), -1)
		text2(TileX+4, TileY-1, "A", color(128, 128, 128))
	end,
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x05] = function () --Horizontal extend
		--if memory.readbyte(0x7E0998) == 0x08 then -- 
			box2(TileX, TileY, 16, 16, 1, color(170, 0, 170), color(85, 0, 85), -1)
			
			text2(TileX+4, TileY-1, "H", color(128, 128, 128))
			--------------------------------------------------------------------
			stack = stack + 1
			local b = memory.readbytesigned(BTS)
			if stack < 16 and (b ~= 0) then
				BTS = BTS + b
				Clip = Clip + bit.lshift(b, 1)
				outlinefunction = outline[bit.rshift(memory.readword(Clip), 12)] or function()
					box2(TileX, TileY, 16, 16, 1, color(170, 0, 170), color(85, 0, 85), -1)
				end
				outlinefunction()
				else
				box2(TileX, TileY, 16, 16, 1, color(170, 0, 170), color(85, 0, 85), -1)
			end
		--end
	end,-- Horizontal extend
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x06] = function () -- X-ray Fluoroscopy is Impossible(XFI)
		box2(TileX, TileY, 16, 16, 1, color(180, 180, 180), color(85, 85, 85), -1)
		text2(TileX+4, TileY-1, "F", color(128, 128, 128))
	end,
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x07] = function () --BaA(Bombable, Air = Bomb bubble sort to Air)(Vert：9313)(Horz：92F9)
		box2(TileX, TileY, 16, 16, 1, color(0, 170, 170), color(0, 85, 85), -1)
		text2(TileX+4, TileY-1, "S", color(255, 255, 255))
	end,
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x08] = function () -- (Solid)
		-- --------------------------------------------------------------------
		box2(TileX, TileY, 16, 16, 1, color(255, 255, 255), color(200, 200, 200), -1)
		-- --------------------------------------------------------------------
		-----box2(TileX, TileY, 16, 16, -1, color(40, 40, 40), color(40, 40, 40), 1)
		----box2(TileX+1, TileY+1, 16-2, 16-2, 1, color(160, 160, 160), color(160, 160, 160), -1)
		---box2(TileX, TileY, 16, 16, 1, color(255, 255, 255), color(255, 255, 255), -1)
		-- --------------------------------------------------------------------
	end,
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x09] = function () -- (Door)
		door = memory.readword(0x8F0000 + memory.readword(0x7E07B5) + 2*bit.band(memory.readbyte(BTS), 0x7F))
		--
		if door == 0x8000 then
			--(Event_Link)
			box2(TileX, TileY, 16, 16, 1, color(255, 0, 0), color(128, 0, 0), -1)
			--text2(TileX+4, TileY-1, "L", color(0, 255, 255))
			-- ----------------------------------------------------------------
			elseif doors[door] then
			--(door_Lnk)
			box2(TileX, TileY, 16, 16, 1, color(255, 0, 255), color(128, 0, 128), -1)
			text2(TileX, TileY-1, "D*", color(0, 255, 255))
			-- ----------------------------------------------------------------
			else
			-- （Elevator_Ele）
			box2(TileX, TileY, 16, 16, 1, color(255, 255, 0), color(128, 128, 0), -1)
			--text2(TileX+4, TileY-1, "E", color(0, 255, 255))
		end
	end, -- Transition block
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x0A] = function () --(Spike)
		--BTS 0x0F is enemy blocks
		--0x03 Grapple draigon
		box2(TileX, TileY, 16, 16, 1, color(255, 0, 0), color(128, 0, 0), -1)
		text2(TileX, TileY-1, "SP", color(255, 0, 0))
	end, -- Spike block
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x0B] = function () -- Crumble block
		--[[
		0x00 - NORMAL
		0x01 - NORMAL DOUBLE
		0x03 - NORMAL QUAD
		0x05 - NORMAL DOUBLE
		0x06 - NORMAL DOUBLE
		
		
		
		0x0E - SPEED
		0x0F - SPEED
		
		0x45 - !!![ANY ITEM]!!!
		
		0x49 - Heal Station
		0x4A - Heal Station
		
		0x4D - Save Station
		
		0x83 - GOLD STATUE HAND
		
		--]]
		box2(TileX, TileY, 16, 16, 1, color(255, 0, 0), color(192, 128, 192))
		text2(TileX, TileY-1, "CR", color(0, 255, 255))
	end, -- Crumble block
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x0C] = function () -- Shoot block (Stb=D) （Door,Gate）
	
		box2(TileX, TileY, 16, 16, 1, color(255, 128, 64), color(128, 64, 32))
		-- ------------------------------------------------------------------
		local b = memory.readbyte(BTS)
		if b >= 0x40 and b <= 0x44 then -- if door
			text2(TileX, TileY-1, "D", color(0, 255, 255))
		else
			-- shoot block
			--[[
			0x00 - NORMAL
			0x01 - NORMAL DOUBLE
			0x04 - NORMAL
			0x05 - NORMAL 2*1
			0x07 - NORMAL 2*2
			
			0x45 - !!![EGG\ITEM BLOCK]!!!
			
			0x09 - SUPER BOMB
			0x0A - SUPER MISSLE
			0x0B - SUPER MISSLE
			
			0x46 - gate shoot Left
			0x47 - gate shoot ?(Right)?
			0x4B - gate GREEN shoot LEFT
			0x4B - gate GREEN shoot ?(Right)?
			0x10 - gate gate
			
			--]]
			box2(TileX, TileY, 16, 16, 1, color(255, 128, 64), color(128, 64, 32))
			text2(TileX, TileY-1, "SB", color(0, 255, 255))
		end
		-- makes doors orange
	end, -- Shot block
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x0D] = function () --（Vertical extend_Ver）
		-- text
		text2(TileX+4, TileY-1, "V", color(128, 128, 128))
		box2(TileX, TileY, 16, 16, 1, color(170, 0, 170), color(85, 0, 85), -1)
		-- ------------------------------------------------------------------
		stack = stack + 1
		local b = memory.readbytesigned(BTS)
		local width = memory.readwordsigned(0x7E07A5)
		if stack < 16 and (b ~= 0) then
			BTS = BTS + b*width
			Clip = Clip + bit.lshift(b*width, 1)
			outlinefunction = outline[bit.rshift(memory.readword(Clip), 12)] or function()
				box2(TileX, TileY, 16, 16, 1, color(170, 0, 170), color(85, 0, 85), -1)
			end
			outlinefunction()
			else
			box2(TileX, TileY, 16, 16, 1, color(170, 0, 170), color(85, 0, 85), -1)
		end
	end, -- Vertical extend
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x0E] = function () -- Grapple block
		-- Icon-style box
		-- Name the layer order (code sequence = display column of the layer)
			-- Outer frame
			box2(TileX, TileY, 16, 16, 1, color(220, 220, 220), color(64, 64, 64), -1)
			-- gray mask
			box2(TileX+1, TileY+1, 14, 14, color(128, 128, 128), color(128, 128, 128))
			--+ Character of the anti-aliasing
			box2(TileX+5, TileY+5, 6, 6, -1, color(96, 96, 96), color(96, 96, 96),-1)
			--box2(TileX+6, TileY+6, 9, 9, 1, color(128, 128, 128), color(96, 96, 96), -1)
			-- +-Shaped hole design
			gui.line(TileX+3, TileY+7, TileX+12, TileY+7, color(0, 0, 0))
			gui.line(TileX+3, TileY+8, TileX+12, TileY+8, color(0, 0, 0))
			gui.line(TileX+7, TileY+3, TileX+7, TileY+12, color(0, 0, 0))
			gui.line(TileX+8, TileY+3, TileX+8, TileY+12, color(0, 0, 0))
			local b = memory.readbyte(BTS)
			if b == 1 then
				gui.line(TileX+16, TileY, TileX, TileY+16, colorA(100, 255, 100,150)) -- broken
			end
	end,
	-- -------------------------------------------------------------------------------------------------------------------------------
	[0x0F] = function () -- Bomb block
		box2(TileX, TileY, 16, 16, 1, color(0, 255, 255), color(0, 128, 128), -1)
		text2(TileX+4, TileY-1, "B", color(0, 255, 255))
	end -- Bomb block
	-- -------------------------------------------------------------------------------------------------------------------------------
}
-- ===================================================================================================================================================

local function BlockDraw(CX,CY)
	local width = memory.readword(0x7E07A5)
	for y=0,15 do
		for x=0,16 do 
			stack = 0
			TileX, TileY = x*16 - bit.band(CX, 0x000F), y*16 - bit.band(CY, 0x000F)
			a = bit.rshift(bit.band(CX+x*16, 0xFFFF), 4) + bit.band(bit.rshift(bit.band(CY+y*16, 0xFFF), 4)*width, 0xFFFF)
			BTS = 0x7F0000 + ((0x6402 + a)%0x10000)
			Clip = 0x7F0000 + ((0x0002 + a*2)%0x10000)
			local ShiftedClip = bit.rshift(memory.readword(Clip), 12)
			
			local outlinefunction = outline[bit.rshift(memory.readword(Clip), 12)] or function()
				box2(TileX, TileY, 16, 16, 1, color(170, 0, 170), color(85, 0, 85), -1)
				text2(TileX+3, TileY-1, "N", color(128, 128, 128))
			end
			outlinefunction()
			
			for _,v in ipairs(RegBlocks) do
				if ShiftedClip == v or DebugFlag then
					--local t = string.format("%02X",bit.band(memory.readword(Clip), 0x00FF))
					local t = string.format("%02X",memory.readbyte(BTS))
					text2(TileX, TileY+7, t, color(255, 165, 0))
					break
				end
			end
			
			
		end
	end
end
local function SamusDraw(ox, oy)
	local radiusX, radiusY = memory.readwordsigned(0x7E0AFE), memory.readwordsigned(0x7E0B00)
	local topleft = {ox - radiusX, oy - radiusY}
	local bottomright = {ox + radiusX, oy + radiusY}
	gui.box(topleft[1], topleft[2], bottomright[1], bottomright[2], 1, color(255,0,0), color(255,0,0), -1)
end
local function EnemyBoxes(cx,cy,num)
	for i = 0, num, 1 do
		local base = 0x0F7A + (i * 0x40)
		local x = memory.readword(base) - cx
		local y = memory.readword(base+ 4) - cy
		local xrad = memory.readbyte(0x7E0F82 + (i * 0x40))
		local yrad = memory.readbyte(0x7E0F84 + (i * 0x40))
		
		hp = memory.readword(base + 0x12)
		box2(x + (xrad * -1),y + (yrad * -1),xrad*2,yrad*2,1,"yellow")
		text2((x-10),(y+7),"HP: ".. hp,"yellow")
	end
	
end
local function PowerBomb(camx,camy)
	local x = memory.readword(0x7E0CE2) - camx
	local y = memory.readword(0x7E0CE4) - camy
	local xrad = bit.band(memory.readbyte(0x7E0CEB),0xFF)
	local yrad = ((xrad / 2) + xrad) / 2
	gui.box(x + (xrad * -1), y + (yrad * -1),x+xrad,y+yrad,0,0xFF00FFA0)
end
local function Projectiles(camx,camy)
	for i=0,9 do
		local projectileX, projectileY = memory.readword(0x0B64 + i*2), memory.readword(0x0B78 + i*2)
		local pradiusX, pradiusY = memory.readwordsigned(0x0BB4 + i*2), memory.readwordsigned(0x0BC8 + i*2)
		local topleft = {projectileX - camx - pradiusX, projectileY - camy - pradiusY}
		local bottomright = {projectileX - camx + pradiusX, projectileY - camy + pradiusY}
		gui.box(topleft[1],topleft[2], bottomright[1],bottomright[2], 0xFFFFFF80, "clear")
		-- draw projectile hitbox
		-- -----------------------------------------------------
		gui.text(topleft[1], topleft[2]-8, memory.readword(0x0C2C + i*2), 0xFFFFFF80, 8)
		-- show projectile damage
		-- -----------------------------------------------------
		if i >= 5 then
			gui.text(topleft[1], topleft[2]-16, memory.readbyte(0x0C7C + i*2), 0xFFFFFF80, 8)
		end
		-- show bomb timer
		-- -----------------------------------------------------
	end

	if bit.band(memory.readbyte(0x7E0CEB),0xFF) > 0 then
		PowerBomb(camx,camy)
	end
end
local function IGtimeFunc()
	local tX,tY = 6,30
	box2(tX,tY,100,10,colorA(0,150,150,150),colorA(0,200,0,255))
	gui.text(tX+2, tY+3, string.format("InGameTime[%2d:%2d:%2d.%2s]", memory.readword(0x09E0), memory.readword(0x09DE), memory.readword(0x09DC), memory.readword(0x09DA)), color(0,255,255))
end
local function SyncControls()
	local keys = input.get()
	if keys["alt"] then 
		if keys["up"] or keys["down"] or keys["left"] or keys["right"] then 
			local Kup,Kdo,Kle,Kri=boolToNum(keys["up"]),boolToNum(keys["down"]),boolToNum(keys["left"]),boolToNum(keys["right"])
			syncX=syncX+Kle*SyncSpeed-Kri*SyncSpeed
			syncY=syncY+Kup*SyncSpeed-Kdo*SyncSpeed
		end
	elseif keys["control"] and keys["up"] then
		if not rest then rest = true end
	elseif keys["control"] and keys["down"] then
		if not SyncState then SyncState = true end
	else
		if rest then 
			rest = false
			syncX,syncY=0,0
			print("Sync restarted")
		end
		if SyncState then 
			SyncState = false
			CamSync = (not CamSync)
			syncX,syncY=0,0
			print("Sync mode changed")
		end
	end
end

-- Main code ==========================================================================================================================================
local DrawTick = function()
	local cur_gamemode = memory.readbyte(0x0998) --in most hacks 0x0998 (maby?)
	--[[ States on 0x0998
	(original states)
	0x00 					nintendo logo
	0x01 					start catscene
	0x2A 					demo
	0x2B 0x2C 0x28 0x29 	loading next demo
	0x04 					saves menu
	0x05					region view
	0x02					option menu
	0x1E 0x1F 				start game text
	0x22 					ceres cutscene
	
	(Gameplay staes)
	0x06 0x07			- pre gameplay loading
	0x08 				- gameplay
	0x0B				- door transaction ceres
	0x0C	 			- is transaction from gameplay to pause
	0x0E				- transactions loading
	0x0F 				- pause menu
	0x10,0x11,0x12		- transaction from pause to gameplay
	]]
	if BGgame then gui.box(0,0,0xFF,0xFF,color(0,0,0)) end -- draw black box on screen
	if cur_gamemode==0x08 or cur_gamemode==0x0C or cur_gamemode==0x12 or cur_gamemode==0x2A then -- if we are in gameplay (2A - demo mode)
	-- CAMERA LOGIC ----------------------------------------------------------------------------------------------------------------
		-- SYNC mode
		local camX,camY = memory.readword(0x0911), memory.readword(0x0915)+1
		local plyX, plyY = memory.readword(0x0AF6), memory.readword(0x0AFA)
		local xS = plyX - camX 
		local yS = plyY - camY
		
		SyncControls()
		if not CamSync then 
			Xoff, Yoff = 130+syncX, 120+syncY else
			Xoff, Yoff = xS, yS
		end
		
		--Cam calc pos
		local cameraX, cameraY = bit.band(plyX-Xoff, 0xFFFF), bit.band(plyY-Yoff, 0xFFFF)  -- 256 224
		if cameraX >= 10000 then cameraX = cameraX-65535 end
		if cameraY >= 10000 then cameraY = cameraY-65535 end
		-- ------------------------------------------------------------------------------------------------------------------------------
		
		BlockDraw(cameraX,cameraY)
		SamusDraw(Xoff, Yoff)
		
		if EnemyDr then EnemyBoxes(cameraX, cameraY,20) end
		if Proj then Projectiles(cameraX,cameraY) end
	
	elseif IGtime and (cur_gamemode==0x0E or cur_gamemode==0x0F or cur_gamemode==0x10 or cur_gamemode==0x11) then -- if we are in pause
		IGtimeFunc()
	end 
	
end 
gui.register(DrawTick)